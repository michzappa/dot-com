:PROPERTIES:
:ID:       6fcb945d-0f8b-429c-990c-91723af18f10
:END:

#+title: GrapheneOS
#+date: <2023-01-08 Sun 12:25>

[[https://grapheneos.org/][Site]]

[[https://matrix.to/#/#grapheneos:grapheneos.org][Matrix]]

A privacy- and security-centric [[id:09ed15b7-e0f0-401b-9d07-f27f7824ba44][Android]] mobile operating system, with particularly enhanced security features. As of <2023-01-06 Fri> this is my main mobile OS. I used the [[https://grapheneos.org/install/web][web installer]] in Chromium to flash my Pixel 7, it worked very well.
