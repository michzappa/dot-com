((nil . ((eval . (progn
                   (setq-local compile-command "nix build --print-build-logs")
                   (setq-local org-link-file-path-type 'relative)
                   (setq-local org-roam-directory
                               (expand-file-name "pages/"
                                                 (locate-dominating-file default-directory ".dir-locals.el")))
                   (setq-local org-roam-db-location
                               (expand-file-name "org-roam.db" org-roam-directory))
                   (setq-local org-roam-capture-templates
                               '(("d" "default" plain "%?" :target
                                  (file+head "${slug}.org" "#+title: ${title}\n#+date: now\n")
                                  :unnarrowed t)))
                   (setq-local org-roam-extract-new-file-path "${slug}.org")
                   (defun mz/update-org-date ()
                     "If an org file has a '#+date' property, update
it to the current date and time."
                     (interactive)
                     (when (derived-mode-p 'org-mode)
                       (save-excursion
                         (widen)
                         (goto-char (point-min))
                         (when (re-search-forward "^#\\+date: " (point-max) t)
                           (progn (delete-region (point) (line-end-position))
                                  (org-insert-time-stamp (current-time) t nil))))))
                   (add-hook 'before-save-hook #'mz/update-org-date)
                   ;; Export text with a specific color.
                   ;; Export part of definition is in publish.el.
                   (with-eval-after-load 'org
                     (org-add-link-type
                      "color"
                      (lambda (path)
                        (message
                         (concat "color " (progn
                                            (add-text-properties
                                             0
                                             (length path)
                                             (list 'face `((t (:foreground ,path))))
                                             path)
                                            path))))
                      (lambda (path desc format)
                        (cond
                         ((eq format 'html)
                          (format "<span style=\"color:%s;\">%s</span>" path desc))))))
                   (defun org-roam-promote-entire-buffer ()
                     "Patched function to add #+date document property."
                     (interactive)
                     (unless (org-roam--buffer-promoteable-p)
                       (user-error "Cannot promote: multiple root headings or there is extra file-level text"))
                     (org-with-point-at 1
                       (let ((title (nth 4 (org-heading-components)))
                             (tags (org-get-tags)))
                         (kill-whole-line)
                         (org-roam-end-of-meta-data t)
                         (insert "#+title: " title "\n")
                         (insert "#+date: now\n") ;; Added this line.
                         (when tags (org-roam-tag-add tags))
                         (org-map-region #'org-promote (point-min) (point-max))
                         (org-roam-db-update-file)))))))))
