{
  description =
    "michzappa's site, created and published with org-roam and emacs.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOs/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, ... }@inputs:
    with inputs;
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = (import nixpkgs { inherit system; });
      in rec {
        publisherEmacs = (pkgs.emacsWithPackages (epkgs:
          (with epkgs; [ htmlize lox-mode nix-mode org-roam racket-mode ])));

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ graphviz publisherEmacs ];
        };

        packages.site = pkgs.stdenv.mkDerivation {
          name = "site";
          src = self;
          buildInputs = [ publisherEmacs ];
          buildPhase = ''
            # Tangle the literate program file.
            emacs ./pages/publishing-this-site.org --batch --funcall org-babel-tangle
            # Publish.
            emacs --batch --load publish.el --funcall mz/publish-site
          '';
          installPhase = ''
            cp -r public $out
          '';
        };

        defaultPackage = self.packages.${system}.site;
      });
}
